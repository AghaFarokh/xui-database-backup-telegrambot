import os
import socket
import time
import telebot

# Replace YOUR_BOT_TOKEN with your actual bot token
bot = telebot.TeleBot("YOUR_BOT_TOKEN")

# Replace YOUR_CHAT_ID with your actual chat ID
chat_id = "YOUR_CHAT_ID"

# Set the directory where the database file is stored
db_dir = "/etc/x-ui"

# Set the filename of the database file
db_filename = "x-ui.db"

# Set the interval (in seconds) for sending the database file
interval = 6 * 60 * 60  # 6 hours

# Get the hostname and public IPv4 address of the server
hostname = socket.gethostname()
ip_addr = os.popen('curl http://api.ipify.org').read()
servername = "YOUR_SERVER_NAME"

while True:
    
    db_path = os.path.join(db_dir, db_filename)

    print("Database file path:", db_path)

    
    if os.path.exists(db_path):
        # Create a message with the hostname, IP address, Servername and database file
        message = f"Server: {hostname} ({ip_addr})\n{servername}\n{time.ctime()}\nDatabase file: {db_filename}"

        with open(db_path, "rb") as file:
            print("Database file opened successfully")
            
            bot.send_document(chat_id, file, caption=message)
            print("Database file sent successfully")

    time.sleep(interval)
