# XUi-Database-BackUp-TelegramBot
This python Telegram bot sends your xui database file to your chat id every 6 hours 

## Getting started

You need to install python, pip and telebot on your server to run this bot

```
sudo apt update
sudo apt install python3
sudo apt install pip
sudo pip install telebot
```

## Installion

Clone this repo on your server
```
git clone https://gitlab.com/AghaFarokh/xui-database-backup-telegrambot
```

Just put YOUR_BOT_TOKEN, YOUR_CHAT_ID and YOUR_SERVER_NAME in the file
You can choose whatever you want for your servername
 
## Run the bot

You should be in the directory where the bot.py file is stored and run this command
```
nohup python3 bot.py &
```